import { defineConfig } from 'vite';
import { fileURLToPath, URL } from 'url';
import react from '@vitejs/plugin-react';
// import cssnano from 'cssnano';
import postCSSPresetEnv from 'postcss-preset-env';
import postCSSImport from 'postcss-import';
// import tailwind from 'tailwindcss';
// import autoprefixer from 'autoprefixer';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  css: {
    postcss: {
      plugins: [
        // tailwind(),
        // autoprefixer(),
        postCSSPresetEnv({
          stage: false,
          features: {
            'custom-media-queries': true,
            'media-query-ranges': true,
            'nesting-rules': true,
          },
        }),
        postCSSImport(),
        // cssnano(),
      ],
    },
  },
  resolve: {
    alias: {
      '~': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  server: {
    port: 2323,
  },
});

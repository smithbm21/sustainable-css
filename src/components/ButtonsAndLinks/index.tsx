import styles from './ButtonsAndLinks.module.css';

export function ButtonsAndLinks() {
  return (
    <div className={styles.grid}>
      <h2>Buttons</h2>
      <h2>Links</h2>

      <div>
        <button>md btn</button>
        <button data-size='sm'>sm btn</button>
        <button data-size='xs'>xs btn</button>
        <br />

        <button data-variant='outline'>md btn</button>
        <button data-variant='outline' data-size='sm'>
          sm btn
        </button>
        <button data-variant='outline' data-size='xs'>
          xs btn
        </button>
        <br />

        <button data-variant='ghost'>md btn</button>
        <button data-variant='ghost' data-size='sm'>
          sm btn
        </button>
        <button data-variant='ghost' data-size='xs'>
          xs btn
        </button>
        <br />

        <button data-variant='destructive'>md btn</button>
        <button data-variant='destructive' data-size='sm'>
          sm btn
        </button>
        <button data-variant='destructive' data-size='xs'>
          xs btn
        </button>
      </div>

      <div>
        <a href='#' data-variant='solid' data-size='md'>
          md link
        </a>
        <a href='#' data-variant='solid' data-size='sm'>
          sm link
        </a>
        <a href='#' data-variant='solid' data-size='xs'>
          xs link
        </a>
        <br />

        <a href='#' data-variant='outline' data-size='md'>
          md link
        </a>
        <a href='#' data-variant='outline' data-size='sm'>
          sm link
        </a>
        <a href='#' data-variant='outline' data-size='xs'>
          xs link
        </a>
        <br />

        <a href='#' data-variant='ghost' data-size='md'>
          md link
        </a>
        <a href='#' data-variant='ghost' data-size='sm'>
          sm link
        </a>
        <a href='#' data-variant='ghost' data-size='xs'>
          xs link
        </a>
        <br />

        <a
          href='#'
          data-variant='destructive'
          data-size='md'
          aria-disabled='true'
        >
          md link
        </a>
        <a href='#' data-variant='destructive' data-size='sm'>
          sm link
        </a>
        <a href='#' data-variant='destructive' data-size='xs'>
          xs link
        </a>
        <br />

        <a href='#' data-size='md'>
          md link
        </a>
        <a href='#' data-size='sm'>
          sm link
        </a>
        <a href='#' data-size='xs'>
          xs link
        </a>
        <a href='#'>default link</a>
      </div>
    </div>
  );
}

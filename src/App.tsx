import { ButtonsAndLinks } from '~/components/ButtonsAndLinks';

export default function App() {
  return (
    <div>
      <h1>ScoutUs Design System</h1>

      <ButtonsAndLinks />
    </div>
  );
}
